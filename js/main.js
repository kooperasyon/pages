(() => {
  // ns-hugo:/home/wayne/Projects/WorkerCoops/kooperasyon/assets/js/gridOverlay.ts
  function findElement(selector) {
    return document.querySelector(selector);
  }
  function ready(fn) {
    if (document.readyState ?? document.readyState !== "loading") {
      fn();
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  }
  function onKeypress(key, fn) {
    document.addEventListener("keypress", function(e) {
      console.log(e);
      if (e.isTrusted && key.key === e.key && e.ctrlKey === e.ctrlKey) {
        fn(e);
      }
    });
  }
  var OverlayElement = class {
    constructor(containerSelector) {
      this.containerSelector = containerSelector;
    }
    listen() {
      console.log("listening...");
      ready(() => {
        console.log("ready done");
        onKeypress({ key: "g", ctrlKey: true }, () => {
          this.toggleOverlay();
        });
      });
    }
    container() {
      return findElement(this.containerSelector);
    }
    overlay() {
      return findElement(`${this.containerSelector} .grid-overlay`);
    }
    showOverlay() {
      if (this.overlay()) {
        this.overlay().style.display = "block";
      } else {
        this.createOverlay();
      }
    }
    hideOverlay() {
      if (this.overlay()) {
        this.overlay().style.display = "none";
      }
    }
    toggleOverlay() {
      if (this.overlayIsVisible()) {
        this.hideOverlay();
      } else {
        this.showOverlay();
      }
    }
    overlayIsVisible() {
      const overlay2 = this.overlay();
      return overlay2 && overlay2.style.display !== "none";
    }
    createOverlay() {
      const overlay2 = document.createElement("div");
      const container = this.container();
      overlay2.classList.add("grid-overlay");
      container.style.position = "relative";
      container.appendChild(overlay2);
    }
  };
  var gridOverlay_default = OverlayElement;

  // <stdin>
  var overlay = new gridOverlay_default(".container");
  overlay.listen();
})();
